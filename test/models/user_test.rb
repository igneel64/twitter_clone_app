require 'test_helper'

class UserTest < ActiveSupport::TestCase
 def setup
  @user=User.new(name: "Example User", email: "user@example.com",
                 password: "foobar", password_confirmation: "foobar")
 end
 
 test "should be valid " do
   assert @user.valid?
 end
 test "should have name" do
  @user.name=""
  assert_not @user.valid?
  
 end
 
 test "should have email" do
   @user.email= ""
   assert_not @user.valid?
 end
 
 test "should not have long mail or name" do
   @user.email="a" * 244 + "@example.com"
   @user.name="pe" * 99 + "er"
   assert_not @user.valid? 
 end
 
 test "should accept valid email" do
  valid_addresses = %w[kuser@example.com USER@FOO.com A_US-ER@FOO.BAR.ORG first.last@foo.jp
  alice+bob@baz.cn]
  valid_addresses.each do |valid_address|
    @user.email=valid_address
    assert @user.valid?, "#{valid_address.inspect} should be valid"
  end
 end
 
 test "should invalid email" do
   invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                         foo@bar_baz.com foo@bar+baz.com]
   invalid_addresses.each do |invalid_address|
    @user.email = invalid_address
    assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
   end
 end
 
 test "email should be unique" do
  duplicate_user = @user.dup
  duplicate_user.email=@user.email.upcase
  @user.save
  assert_not duplicate_user.valid?
   
 end
 
 test "password should have minimum length" do
  @user.password=@user.password_confirmation="a"*5
  assert_not @user.valid?
 end
 
 test "authenticated? should return false for a user nil digest" do
  assert_not @user.authenticated?(:remember, '')
 end
 
 test "associated microposts should be destroyed" do
  @user.save
  @user.microposts.create!(content: "Lorem ipsum")
  assert_difference 'Micropost.count', -1 do
   @user.destroy
  end
 end
 
 test "should follow and unfollow a user" do
  michael= users(:michael)
  archer=users(:archer)
  assert_not michael.following?(archer)
  michael.follow(archer)
  assert archer.followers.include?(michael)
  assert michael.following?(archer)
  michael.unfollow(archer)
  assert_not michael.following?(archer)
 end
 
 test "feed should have the right posts" do
  michael=users(:michael)
  archer=users(:archer)
  lana=users(:lana)
  #lana is followed by michael
  lana.microposts.each do |post_following|
   assert michael.feed.include?(post_following)
  end
  #self posts
  michael.microposts.each do |post_self|
   assert michael.feed.include?(post_self)
  end
  #michael does not follow archer
  archer.microposts.each do |post_unfollowed|
   assert_not michael.feed.include?(post_unfollowed)
  end
  
 end
 
end
